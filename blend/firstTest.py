# module purge
# module load venv/ase
# module load openmpi
# module load pylammps

# from lammps import lammps #, PyLammps

# lmp = lammps() # (cmd_args) # initiate LAMMPS object with command line args

# lmp.scatter_atoms('x',1,3,positions) # atom coords to LAMMPS C array
# lmp.command(cmd) # executes a one line cmd string
# lmp.extract_variable(...) # extracts a per atom variable
# lmp.extract_global(...) # extracts a global variable
# lmp.close() # close the lammps object

from ase import Atom, Atoms
from ase.build import bulk
from ase.calculators.lammpslib import LAMMPSlib

cmds = ["pair_style eam/alloy",
        "pair_coeff * * NiAlH_jea.eam.alloy Ni H"]

Ni = bulk('Ni', cubic=True)
H = Atom('H', position=Ni.cell.diagonal()/2)
NiH = Ni + H

lammps = LAMMPSlib(lmpcmds=cmds, log_file='test.log')

NiH.calc = lammps
print("Energy ", NiH.get_potential_energy())