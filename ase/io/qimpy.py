"""
This module contains functionality for reading and writing an ASE
Atoms object in qimpy yaml format.

"""

import re

import numpy as np

from ase import Atoms
from ase.utils import reader, writer
from ase.io.utils import ImageIterator
from ase.io import ParseError
from pathlib import Path
import yaml

__all__ = [
    'write_qimpy'
]



def _write_qimpy(fd, atoms):
    """Write a block of positions for qimpy yaml file

    Args:
        fd (fd): writeable Python file descriptor
        atoms (ase.Atoms): Atoms to write
        #index (int): configuration number written to block header

    """
    # Obtain lattice parameters and structure

    # Get lattice vectors (3x3 array):
    lattice = atoms.get_cell()[:] #* angstrom

    # Get atomic positions
    positions = atoms.get_scaled_positions()

    # Get symbols
    symbols = atoms.get_chemical_symbols()

    lattice_dict = {
        "vector1": lattice[0].tolist(),
        "vector2": lattice[1].tolist(),
        "vector3": lattice[2].tolist(),
        "movable": False,
    }


    coordinates = [
        [symbol] + position.tolist() for symbol, position in zip(symbols, positions)
    ]
    ions = {
        "fractional": True,
        "pseudopotentials": 'PATH/TO',
        "coordinates": coordinates,
    }

    genDataDict = {
        "lattice": lattice_dict,
        "ions": ions,
    }

    with open(fd, "w", encoding = "utf-8") as yaml_file:
        dump = yaml.dump(genDataDict, default_flow_style = False, sort_keys = False, allow_unicode = True, encoding = None)
        yaml_file.write(dump)
    