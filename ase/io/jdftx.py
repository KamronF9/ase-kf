"""
This module contains functionality for reading and writing an ASE
Atoms object in jdftx formats.

"""

import re

import numpy as np

from ase import Atoms
from ase.utils import reader, writer
from ase.io.utils import ImageIterator
from ase.io import ParseError
from .vasp_parsers import vasp_outcar_parsers as vop
from pathlib import Path
from ase.units import Bohr, Hartree

__all__ = [
    'write_jdftx_IonLattInputs'
]
# add later? 'read_jdftx', 'read_jdftx_out',

#based on vasp.py format io script

@writer
def write_jdftx_IonLattInputs(
                            baseFilename,
                            atoms):
    """ Constructs a JDFTx ionpos and lattice files using the input atoms.  """
    inputfile = ''

    # Add lattice info
    R = atoms.get_cell() / Bohr
    inputfile += 'lattice \\\n'
    for i in range(3):
        for j in range(3):
            inputfile += '%f  ' % (R[j, i])
        if(i != 2):
            inputfile += '\\'
        inputfile += '\n'
    # write lattice
    with open(baseFilename.name + '.lattice','w') as f:
        f.write(inputfile)

    # reset input
    inputfile = ''
    # Add ion info
    atomPos = [x / Bohr for x in list(atoms.get_positions())]  # Also convert to bohr
    atomNames = atoms.get_chemical_symbols()   # Get element names in a list
    inputfile += '\ncoords-type cartesian\n'
    for i in range(len(atomPos)):
        inputfile += 'ion %s %f %f %f \t 1\n' % (atomNames[i], atomPos[i][0], atomPos[i][1], atomPos[i][2])
    # write lattice
    with open(baseFilename.name + '.ionpos','w') as f:
        f.write(inputfile)

