from ase import Atoms
from ase.io import read, write
from ase.io.qimpy import _write_qimpy
from ase.build import bulk

d = 2.9
L = 10.0
atoms = Atoms('Au',
             positions=[[0, L / 2, L / 2]],
             cell=[d, L, L],
             pbc=[1, 0, 0])

_write_qimpy('test1.yaml',atoms)

atoms2 = read('poscarFile', format='vasp')
#, format='poscar'
_write_qimpy('test2.yaml',atoms2)